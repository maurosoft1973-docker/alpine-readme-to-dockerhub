ARG DOCKER_ALPINE_VERSION

FROM maurosoft1973/alpine:$DOCKER_ALPINE_VERSION

ARG BUILD_DATE
ARG ALPINE_ARCHITECTURE
ARG ALPINE_RELEASE
ARG ALPINE_VERSION
ARG ALPINE_VERSION_DATE

LABEL \
    maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    architecture="$ALPINE_ARCHITECTURE" \
    alpine-version="$ALPINE_VERSION" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="alpine-readme-to-dockerhub" \
    org.opencontainers.image.description="Push Readme to Docker Hub with Docker image running on Alpine Linux" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/alpine-readme-to-dockerhub" \
    org.opencontainers.image.source="https://gitlab.com/maurosoft1973-docker/alpine-readme-to-dockerhub" \
    org.opencontainers.image.created=$BUILD_DATE

RUN \
    echo "" > /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/main" >> /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/community" >> /etc/apk/repositories && \
    apk update && \
    apk add --update --no-cache curl jq ca-certificates gettext && \
    cp /usr/bin/envsubst /usr/local/bin/

ADD files/run-alpine-readme-to-dockerhub.sh /scripts/run-alpine-readme-to-dockerhub.sh

RUN chmod +x /scripts/run-alpine-readme-to-dockerhub.sh

VOLUME ["/data"]

ENTRYPOINT ["/scripts/run-alpine-readme-to-dockerhub.sh"]