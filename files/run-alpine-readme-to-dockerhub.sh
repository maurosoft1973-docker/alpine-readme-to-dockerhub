#!/bin/sh

DOCKERHUB_USERNAME=${DOCKERHUB_USERNAME:-""}
DOCKERHUB_PASSWORD=${DOCKERHUB_PASSWORD:-""}
DOCKERHUB_REPO_PREFIX=${DOCKERHUB_REPO_PREFIX:-""}
DOCKERHUB_REPO_NAME=${DOCKERHUB_REPO_NAME:-""}
DOCKERHUB_REPO_URL="https://hub.docker.com/v2/repositories/$DOCKERHUB_REPO_PREFIX/$DOCKERHUB_REPO_NAME/"
FULL_DESCRIPTION=${FULL_DESCRIPTION:-""}
SHORT_DESCRIPTION=${SHORT_DESCRIPTION:-""}

ARGUMENT_ERROR=0

if [ "${DOCKERHUB_USERNAME}" == "" ]; then
    echo "ERROR: The environment variable DOCKERHUB_USERNAME is not set!"
    ARGUMENT_ERROR=1
fi

if [ "${DOCKERHUB_PASSWORD}" == "" ]; then
    echo "ERROR: The environment variable DOCKERHUB_PASSWORD is not set!"
    ARGUMENT_ERROR=1
fi

if [ "${DOCKERHUB_REPO_PREFIX}" == "" ]; then
    echo "ERROR: The environment variable DOCKERHUB_REPO_PREFIX is not set!"
    ARGUMENT_ERROR=1
fi

if [ "${DOCKERHUB_REPO_NAME}" == "" ]; then
    echo "ERROR: The environment variable DOCKERHUB_REPO_NAME is not set!"
    ARGUMENT_ERROR=1
fi

if [ ${ARGUMENT_ERROR} -ne 0 ]; then
    exit 1
fi

source /scripts/init-alpine.sh

TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${DOCKERHUB_USERNAME}'", "password": "'${DOCKERHUB_PASSWORD}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)

if [ "$TOKEN" != "null" ]; then
    echo "Login OK"
    REPO_URL="https://hub.docker.com/v2/repositories/$DOCKERHUB_REPO_PREFIX/$DOCKERHUB_REPO_NAME/"
    echo "Send Full description"
    RESPONSE=$(curl -s -H "Authorization: JWT ${TOKEN}" -X PATCH --data-urlencode full_description="${FULL_DESCRIPTION}" ${REPO_URL})
    echo "Send Short description"
    RESPONSE=$(curl -s -H "Authorization: JWT ${TOKEN}" -X PATCH --data-urlencode description="${SHORT_DESCRIPTION}" ${REPO_URL})
else 
    echo "Login Failed"
    exit 1
fi;
