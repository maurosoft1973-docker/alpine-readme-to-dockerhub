# README to Docker Hub

[![Docker Automated build](https://img.shields.io/docker/automated/maurosoft1973/alpine-readme-to-dockerhub.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-readme-to-dockerhub/)
[![Docker Pulls](https://img.shields.io/docker/pulls/maurosoft1973/alpine-readme-to-dockerhub.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-readme-to-dockerhub/)
[![Docker Stars](https://img.shields.io/docker/stars/maurosoft1973/alpine-readme-to-dockerhub.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-readme-to-dockerhub/)
[![Alpine Version](https://img.shields.io/badge/Alpine%20version-v%ALPINE_VERSION%-green.svg?style=for-the-badge)](https://alpinelinux.org/)

This Docker image [(maurosoft1973/alpine-readme-to-dockerhub)](https://hub.docker.com/r/maurosoft1973/alpine-readme-to-dockerhub/) is based on the minimal [Alpine Linux](https://alpinelinux.org/).

##### Alpine Version %ALPINE_VERSION% (Released %ALPINE_VERSION_DATE%)

----

## Description

This is a quick hack to push README.md files and short description to Docker hub.

```console
FULL_DESCRIPTION=$(if [ -f "/path/to/README.md" ]; then cat "/path/to/README"; else echo ""; fi)

docker run --rm \
    -e DOCKERHUB_USERNAME=username \
    -e DOCKERHUB_PASSWORD=password \
    -e DOCKERHUB_REPO_PREFIX=organization \
    -e DOCKERHUB_REPO_NAME=repository \
    -e FULL_DESCRIPTION="$FULL_DESCRIPTION"
    -e SHORT_DESCRIPTION="Short Description of image"
     maurosoft1973/alpine-readme-to-dockerhub
```
That's it.

The user you use to push the README.md need to be admin of the repository.


## Architectures

* ```:aarch64``` - 64 bit ARM
* ```:armhf```   - 32 bit ARM v6
* ```:armv7```   - 32 bit ARM v7
* ```:ppc64le``` - 64 bit PowerPC
* ```:x86```     - 32 bit Intel/AMD
* ```:x86_64```  - 64 bit Intel/AMD (x86_64/amd64)


## Tags

* ```:latest```         latest branch based (Automatic Architecture Selection)
* ```:aarch64```        latest 64 bit ARM
* ```:armhf```          latest 32 bit ARM v6
* ```:armv7```          latest 32 bit ARM v7
* ```:ppc64le```        latest 64 bit PowerPC
* ```:x86```            latest 32 bit Intel/AMD
* ```:x86_64```         latest 64 bit Intel/AMD
* ```:test```           test branch based (Automatic Architecture Selection)
* ```:test-aarch64```   test 64 bit ARM
* ```:test-armhf```     test 32 bit ARM v6
* ```:test-armv7```     test 32 bit ARM v7
* ```:test-ppc64le```   test 64 bit PowerPC
* ```:test-x86```       test 32 bit Intel/AMD
* ```:test-x86_64```    test 64 bit Intel/AMD
* ```:%ALPINE_VERSION%``` %ALPINE_VERSION% branch based (Automatic Architecture Selection)
* ```:%ALPINE_VERSION%-aarch64```   %ALPINE_VERSION% 64 bit ARM
* ```:%ALPINE_VERSION%-armhf```     %ALPINE_VERSION% 32 bit ARM v6
* ```:%ALPINE_VERSION%-armv7```     %ALPINE_VERSION% 32 bit ARM v7
* ```:%ALPINE_VERSION%-ppc64le```   %ALPINE_VERSION% 64 bit PowerPC
* ```:%ALPINE_VERSION%-x86```       %ALPINE_VERSION% 32 bit Intel/AMD
* ```:%ALPINE_VERSION%-x86_64```    %ALPINE_VERSION% 64 bit Intel/AMD


## Layers & Sizes
| Version                                                                               | Size                                                                                                                 |
|---------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| ![Version](https://img.shields.io/badge/version-amd64-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-readme-to-dockerhub/latest?style=for-the-badge)  |
| ![Version](https://img.shields.io/badge/version-armv6-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-readme-to-dockerhub/armhf?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-armv7-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-readme-to-dockerhub/armv7?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-ppc64le-blue.svg?style=for-the-badge) | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-readme-to-dockerhub/ppc64le?style=for-the-badge) |
| ![Version](https://img.shields.io/badge/version-x86-blue.svg?style=for-the-badge)     | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-readme-to-dockerhub/x86?style=for-the-badge)     |


## Environment Variables:

|Available variables     |Default value        |Description                                         |
|------------------------|---------------------|----------------------------------------------------|
|`LC_ALL`                |en_GB.UTF-8          |Set locale                                          |
|`TIMEZONE`              |Europe/Brussels      |Set timezone                                        |
|`SHELL_TERMINAL`        |bin/sh               |Set shell                                           |
|`DOCKERHUB_USERNAME`    |no default           |The Username (not mail address) used to authenticate|
|`DOCKERHUB_PASSWORD`    |no default           |Password of the `DOCKERHUB_USERNAME`-user           |
|`DOCKERHUB_REPO_PREFIX` |no default           |Organization or username for the repository         |
|`DOCKERHUB_REPO_NAME`   |no default           |Name of the repository you want to push to          |
|`FULL_DESCRIPTION`      |no default           |Full description for the Dockerhub repo             |
|`SHORT_DESCRIPTION`     |no default           |Short description for the Dockerhub repo            |


## Example of use

### 1.Example for update short description and full description with GITLAB Pipeline.

Your define the variables $DOCKER_HUB_USER and $DOCKER_HUB_PASSWORD (https://gitlab.com/help/ci/variables/README#variables) into "Settings -> CI/CD -> Variables" on the repository

```yaml
image: docker:stable

stages:
  - readme

readme:
  stage: readme
  variables:
    DOCKERHUB_USERNAME: "$DOCKER_HUB_USER"
    DOCKERHUB_PASSWORD: "$DOCKER_HUB_PASSWORD"
    DOCKERHUB_REPO_PREFIX: "myorganization"
    DOCKERHUB_REPO_NAME: "myrepository"
  script:
  - FULL_DESCRIPTION=$(if [ -f "$(pwd)/README.md" ]; then cat "$(pwd)/README.md"; else echo ""; fi)
  - docker pull maurosoft1973/alpine-readme-to-dockerhub:latest
  - docker run --rm -e DOCKERHUB_USERNAME="$DOCKERHUB_USERNAME" -e DOCKERHUB_PASSWORD="$DOCKERHUB_PASSWORD" -e DOCKERHUB_REPO_PREFIX="$DOCKERHUB_REPO_PREFIX" -e DOCKERHUB_REPO_NAME="$DOCKERHUB_REPO_NAME" -e SHORT_DESCRIPTION="Your short description" -e FULL_DESCRIPTION="$FULL_DESCRIPTION" maurosoft1973/alpine-readme-to-dockerhub
```
***
###### Last Update %LAST_UPDATE%